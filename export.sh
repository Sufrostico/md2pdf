#! /bin/bash

# @author: sufrostico

srctemplates="$(dirname $0)/templates/"
srctemplates="/home/sufrostico/A_TEC/C_Laboratorio/Proyectos/md2pdf/templates/"

template=$1
srcfile=$2

mostrar_ayuda() { 
	echo "md+plantilla -> pdf"
	echo ""
	echo "Este comando se debe ejecutar con los siguientes argumentos:" 
    echo -e "\nUso:\n$(basename $0) [Nombre de la plantilla] [Archivo .md] \n" 
	} 

# Menos de dos argumentos
	if [  $# -le 1 ] 
	then 
		mostrar_ayuda
		exit 1
	fi 
 
# soporte para las opciones --help y -h
	if [[ ( $# == "--help") ||  $# == "-h" ]] 
	then 
        mostrar_ayuda
		exit 0
	fi 


# Directorio de salida
outdir="$(dirname $srcfile)/"

#archivo de salida
onlyfile="$(basename $srcfile)"
outfile="${onlyfile%.*}"

# carpeta temporal de procesamiento
tmpdir="/tmp/$outfile"

# crea el directorio temporal
mkdir -p $tmpdir

# limpieza del md
sed '2{/^$/d;}' "$outfile.md"           > "$tmpdir/filtrado.md"
sed '/div\>/d'  "$tmpdir/filtrado.md"   > "$tmpdir/final.md"

#copia las imágenes
cp -r "$srctemplates/$template/" $tmpdir

# Convertido de md a tex usando la plantilla especificada
pandoc -s -N --chapters --template="$srctemplates/$template.tex" "$tmpdir/final.md" -o "$tmpdir/$outfile.tex"

#limpieza de los archivos .tex
# sed -i 's/includegraphics{/includegraphics\[width=7.0cm\]{/g'   "$tmpdir/$outfile.tex"
# sed -i 's/begin{figure}\[htbp\]/begin{figure}\[H\]/g'           "$tmpdir/$outfile.tex"

echo "----> inicio de pdflatex"

# convertir del archivo .tex a pdf
current=$(pwd)

cd $tmpdir
echo "pdflatex -halt-on-error $outfile.tex > $outfile.pdf"
pdflatex -halt-on-error "$outfile.tex" #> "$outfile.pdf"
pdflatex -halt-on-error "$outfile.tex" #> "$outfile.pdf"
echo "cp -r $outfile.pdf $current"
cp -r "$outfile.pdf" $current

echo "----> final de pdflatex"

#rm -rf $tmpdir

#rm $outfile.log
#rm $outfile.aux
#rm $outfile.out
